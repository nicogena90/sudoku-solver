# Sudoku solver

This type-script app resolves any kind of sudoku that exists, including the ones with several solutions.

It uses several strategies to solve the sudoku, from checking unique posible value in the space to guessing posible answers with recursive functions.

## Sudoku fomrat

The format of the parameter that the app expects the sudoku is an 81 length number array that reads teh sudoku row by row

### Example

[0, 0, 4, 0, 5, 0, 0, 0, 0, 9, 0, 0, 7, 3, 4, 6, 0, 0, 0, 0, 3, 0, 2, 1, 0, 4, 9, 0, 3, 5, 0, 9, 0, 4, 8, 0, 0, 9, 0, 0, 0, 0, 0, 3, 0, 0, 7, 6, 0, 1, 0, 9, 2, 0, 3, 1, 0, 9, 7, 0, 2, 0, 0, 0, 0, 9, 1, 8, 2, 0, 0, 3, 0, 0, 0, 0, 6, 0, 1, 0, 0,]

the response with the complete sudoku will,be

[2, 6, 4, 9, 8, 1, 7, 5, 3]
[8, 5, 9, 7, 3, 4, 6, 2, 1]
[3, 1, 7, 6, 5, 2, 8, 4, 9]
[1, 3, 5, 8, 9, 2, 4, 7, 6]
[2, 9, 7, 5, 4, 6, 3, 1, 8]
[4, 8, 6, 7, 3, 1, 9, 2, 5]
[3, 1, 8, 6, 4, 9, 5, 2, 7]
[9, 7, 5, 1, 8, 2, 4, 6, 3]
[2, 6, 4, 5, 7, 3, 1, 9, 8]

## Run App

With script

```
sh dev-init.sh
```

OR

Step by step

1. Go to the api folder of the app

```
cd sudoku-solver/api
```

2. Install dependencies

```
npm i
```

3. Start api

```
npm run start-dev
```

## Run test

1. Go to the api folder of the app

```
cd sudoku-solver/api
```

2. Run Tests api

```
npm test
```

## Apis examples

- Adds a new line in the log

```
GET  localhost:5000/log/get
{
	"response": {
		"data": {
			"total_time": "192.170664 ms",
			"result": [
				[2, 6, 4, 9, 8, 1, 7, 5, 3],
				[8, 5, 9, 7, 3, 4, 6, 2, 1],
				[3, 1, 7, 6, 5, 2, 8, 4, 9],
				[1, 3, 5, 8, 9, 2, 4, 7, 6],
				[2, 9, 7, 5, 4, 6, 3, 1, 8],
				[4, 8, 6, 7, 3, 1, 9, 2, 5],
				[3, 1, 8, 6, 4, 9, 5, 2, 7],
				[9, 7, 5, 1, 8, 2, 4, 6, 3],
				[2, 6, 4, 5, 7, 3, 1, 9, 8]
			]
		}
	}
}
```

## Notes

This tempalte is made in typescript and were used the followings libraries:

- express https://www.npmjs.com/package/express
- @types/node https://www.npmjs.com/package/@types/node
- @types/express https://www.npmjs.com/package/@types/express
- jest https://www.npmjs.com/package/jest
