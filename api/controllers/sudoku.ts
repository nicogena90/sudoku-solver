import { Response } from "express";
import * as httpResp from "./httpResponses";
import * as sudokuService from "../services/sudoku";

const FILE_LOG = "controllers/log.ts";

export async function resolve(req: any, res: Response) {
  try {
    const hrstart = process.hrtime();
    const { values } = req.body;

    if (!values) {
      httpResp.error(
        res,
        app.httpResponses.errorCode.BAD_REQUEST,
        `The sudoku is missing`
      );
    }

    const newSudokuBlocks = await sudokuService.resolve(values);

    const hrend = process.hrtime(hrstart);

    httpResp.success(res, {
      total_time: `${hrend[1] / 1000000} ms`,
      result: newSudokuBlocks,
    });
  } catch (error) {
    httpResp.error(
      res,
      app.httpResponses.errorCode.INTERNAL_SERVER_ERROR,
      `${FILE_LOG} (add): error (${error.stack})`
    );
  }
}
