import * as logController from "../controllers/sudoku";

var express = require("express");
var api = express("sudoku");

api.post("", logController.resolve);

export default api;
