import * as express from "express";
const app = express();

import logRoutes from "./routes/sudoku";

app.use(express.json({ limit: "50mb" }));

// Load the routes
app.use("/sudoku/", logRoutes);

export default app;
