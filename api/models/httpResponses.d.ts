declare namespace app.httpResponses {
  export const enum errorCode {
    OK = 200,
    BAD_REQUEST = 400,
    FORBIDDEN = 403,
    NOT_FOUND = 404,
    INTERNAL_SERVER_ERROR = 500,
  }

  export const enum errorMsgs {
    MISSING_MESSAGE_FIELD = "The message is a mandatory field",
  }

  export interface response {
    data?: any;
    errorMsg?: string;
  }
}
