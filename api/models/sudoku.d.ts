declare namespace app.sudoku {
  export interface sudoku {
    rows: number[][];
    columns: number[][];
    blocks: number[][];
  }

  export interface solution {
    blockCount: number;
    rowIndex?: number;
    columnIndex?: number;
    spaceIndex?: number;
    solution?: number[];
    counter?: number;
  }
}
