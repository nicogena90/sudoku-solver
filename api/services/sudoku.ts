export async function resolve(values: number[]) {
  let sudoku = initalizeSudoku(values);

  let blocks = await completeResolve(sudoku.blocks);

  const finalResult = await guessSolution(blocks);

  if (!isSudokuResolved(finalResult)) {
    throw new Error("A solution was not found");
  }

  return finalResult;
}

async function solveByOnlyOption(blocks: number[][]) {
  let sudokuCopy;
  do {
    sudokuCopy = blocks.map((arr) => arr.slice());
    let sudoku = getCompleteSudokuFromBlock(blocks);
    blocks = await resolveBlock(sudoku);
    sudoku = getCompleteSudokuFromBlock(blocks);
  } while (!areSudokuEqual(blocks, sudokuCopy));

  return blocks;
}

function areSudokuEqual(blocksOne: number[][], blocksTwo: number[][]) {
  for (let blockIndex = 0; blockIndex < blocksOne.length; blockIndex++) {
    for (
      let spaceIndex = 0;
      spaceIndex < blocksOne[blockIndex].length;
      spaceIndex++
    ) {
      if (
        blocksOne[blockIndex][spaceIndex] !== blocksTwo[blockIndex][spaceIndex]
      ) {
        return false;
      }
    }
  }
  return true;
}

async function guessSolution(blocks: number[][]): Promise<number[][]> {
  if (isSudokuResolved(blocks)) {
    return blocks;
  }
  let sudoku = getCompleteSudokuFromBlock(blocks);
  const newSolutions = await getXPossibleSolutions(blocks, sudoku);

  if (!newSolutions || newSolutions.length === 0) {
    return blocks;
  }

  const { blockCount, spaceIndex, solution } = newSolutions[0];

  let sudokuCopy = blocks.map((arr) => arr.slice());

  for (
    let newSolutionIndex = 0;
    newSolutionIndex < solution.length;
    newSolutionIndex++
  ) {
    blocks[blockCount][spaceIndex] = solution[newSolutionIndex];

    blocks = await completeResolve(blocks);

    if (isSudokuResolved(blocks)) {
      return blocks;
    }

    if (isSudokuWrong(blocks)) {
      blocks = sudokuCopy.map((arr) => arr.slice());
      continue;
    }

    const posibleResult = await guessSolution(blocks);

    if (isSudokuResolved(posibleResult)) {
      return posibleResult;
    } else {
      blocks = sudokuCopy.map((arr) => arr.slice());
    }
  }

  return sudokuCopy;
}

function getCompleteSudokuFromBlock(block: number[][]) {
  let sudokuLine = sudokuToNumberLine(block);
  return initalizeSudoku(sudokuLine);
}

async function completeResolve(blocks: number[][]) {
  let blocksCopy: number[][] = [];
  do {
    blocksCopy = blocks.map((arr) => arr.slice());
    blocks = await solveByOnlyOption(blocks);

    if (isSudokuResolved(blocks)) {
      return blocks;
    }

    blocks = await solveByUniquePosible(blocks);

    if (isSudokuResolved(blocks)) {
      return blocks;
    }

    blocks = await solveByColumnRowHiddenValue(blocks);

    if (isSudokuResolved(blocks)) {
      return blocks;
    }
  } while (!areSudokuEqual(blocks, blocksCopy));

  return blocks;
}

function isSudokuWrong(blocks: number[][]) {
  for (const block of blocks) {
    if (block.includes(undefined)) return true;
  }
  return false;
}

async function solveByColumnRowHiddenValue(blocks: number[][]) {
  const result: app.sudoku.solution[] = [];
  const allSolutions = (await getAllPossibleSolutions(blocks)).filter(
    (sol) => sol.solution !== []
  );

  for (let index = 0; index < 9; index++) {
    const columnsSolutions = allSolutions.filter(
      (s) => s.columnIndex === index
    );

    const rowsSolutions = allSolutions.filter((s) => s.rowIndex === index);

    result.push.apply(result, GetSolutionsByRowOrColumn(columnsSolutions));
    result.push.apply(result, GetSolutionsByRowOrColumn(rowsSolutions));
  }

  for (const uniqueSolution of result) {
    if (uniqueSolution.blockCount !== undefined)
      blocks[uniqueSolution.blockCount][uniqueSolution.spaceIndex] =
        uniqueSolution.solution[0];
  }

  blocks = await solveByOnlyOption(blocks);

  return blocks;
}

function GetSolutionsByRowOrColumn(solutions: app.sudoku.solution[]) {
  const result = [];
  const solutionCounter = [];
  for (const solution of solutions) {
    for (const iterator of solution.solution) {
      const findSolution = solutionCounter.filter(
        (item) => item.solution[0] === iterator
      )[0];

      if (findSolution) {
        findSolution.counter += 1;
      } else {
        solutionCounter.push({
          rowIndex: solution.rowIndex,
          columnIndex: solution.columnIndex,
          blockCount: solution.blockCount,
          spaceIndex: solution.spaceIndex,
          counter: 1,
          solution: [iterator],
        });
      }
    }
  }
  result.push(solutionCounter.filter((item) => item.counter === 1));

  return result;
}

async function solveByUniquePosible(blocks: number[][]) {
  let sudokuCopy: number[][] = [];

  do {
    sudokuCopy = blocks.map((arr) => arr.slice());

    const allSolutions = await getAllPossibleSolutions(blocks);

    const uniqueSolutions = checkUniqueSolutions(allSolutions);

    for (const uniqueSolution of uniqueSolutions) {
      blocks[uniqueSolution.blockCount][uniqueSolution.spaceIndex] =
        uniqueSolution.solution[0];
    }

    blocks = await solveByOnlyOption(blocks);

    if (isSudokuResolved(blocks)) {
      return blocks;
    }
  } while (!areSudokuEqual(blocks, sudokuCopy));

  return blocks;
}

function checkUniqueSolutions(solution: app.sudoku.solution[]) {
  const result: app.sudoku.solution[] = [];
  for (let blockNumber = 0; blockNumber < 9; blockNumber++) {
    const currentBlockSolution = solution.filter(
      (solution) => solution.blockCount === blockNumber
    );

    const solutionCounter: app.sudoku.solution[] = [];

    for (let index = 0; index < currentBlockSolution.length; index++) {
      for (const iterator of currentBlockSolution[index].solution) {
        const findSolution = solutionCounter.filter(
          (item) => item.solution[0] === iterator
        )[0];

        if (findSolution) {
          findSolution.counter += 1;
        } else {
          solutionCounter.push({
            rowIndex: currentBlockSolution[index].rowIndex,
            columnIndex: currentBlockSolution[index].columnIndex,
            blockCount: currentBlockSolution[index].blockCount,
            spaceIndex: currentBlockSolution[index].spaceIndex,
            counter: 1,
            solution: [iterator],
          });
        }
      }
    }
    result.push.apply(
      result,
      solutionCounter.filter((item) => item.counter === 1)
    );
  }

  return result;
}

// no tiene sentido los parametro
async function getAllPossibleSolutions(blocks: number[][]) {
  const result = [];
  let sudoku = getCompleteSudokuFromBlock(blocks);

  let blockCount = 0;
  for (let rowIndex = 0; rowIndex < 9; rowIndex += 3) {
    for (let columnIndex = 0; columnIndex < 9; columnIndex += 3) {
      const block: number[] = blocks[blockCount];
      const solution = await getSolutionBlock(
        block,
        [
          sudoku.rows[rowIndex],
          sudoku.rows[rowIndex + 1],
          sudoku.rows[rowIndex + 2],
        ],
        [
          sudoku.columns[columnIndex],
          sudoku.columns[columnIndex + 1],
          sudoku.columns[columnIndex + 2],
        ]
      );

      for (let index = 0; index < 9; index++) {
        if (solution[index].length !== 1)
          result.push({
            blockCount,
            rowIndex: getRowBySpaceAndBlock(blockCount, index),
            columnIndex: getColumnBySpaceAndBlock(blockCount, index),
            spaceIndex: index,
            solution: solution[index],
          });
      }

      blockCount += 1;
    }
  }

  return result;
}

async function getXPossibleSolutions(
  sudokuBlocks: number[][],
  sudoku: app.sudoku.sudoku
) {
  const result = [];

  let blockCount = 0;
  for (let rowIndex = 0; rowIndex < 9; rowIndex += 3) {
    for (let columnIndex = 0; columnIndex < 9; columnIndex += 3) {
      const block: number[] = sudokuBlocks[blockCount];
      const solution = await getSolutionBlock(
        block,
        [
          sudoku.rows[rowIndex],
          sudoku.rows[rowIndex + 1],
          sudoku.rows[rowIndex + 2],
        ],
        [
          sudoku.columns[columnIndex],
          sudoku.columns[columnIndex + 1],
          sudoku.columns[columnIndex + 2],
        ]
      );

      for (let index = 0; index < 9; index++) {
        if ([2, 3].includes(solution[index].length))
          result.push({
            blockCount,
            spaceIndex: index,
            solution: solution[index],
          });
      }
      blockCount += 1;
    }
  }

  return result;
}

function getColumnBySpaceAndBlock(blockCount: number, spaceIndex: number) {
  switch (blockCount) {
    case 0:
    case 3:
    case 6:
      if ([0, 3, 6].includes(spaceIndex)) {
        return 0;
      }

      if ([1, 4, 7].includes(spaceIndex)) {
        return 1;
      }

      return 2;

      break;

    case 1:
    case 4:
    case 7:
      if ([0, 3, 6].includes(spaceIndex)) {
        return 3;
      }

      if ([1, 4, 7].includes(spaceIndex)) {
        return 4;
      }

      return 5;

      break;

    case 2:
    case 5:
    case 8:
      if ([0, 3, 6].includes(spaceIndex)) {
        return 6;
      }

      if ([1, 4, 7].includes(spaceIndex)) {
        return 7;
      }

      return 8;

      break;
  }
}

function getRowBySpaceAndBlock(blockCount: number, spaceIndex: number) {
  switch (blockCount) {
    case 0:
    case 1:
    case 2:
      if (spaceIndex < 3) {
        return 0;
      }

      if (spaceIndex < 6) {
        return 1;
      }

      return 2;

      break;

    case 3:
    case 4:
    case 5:
      if (spaceIndex < 3) {
        return 3;
      }

      if (spaceIndex < 6) {
        return 4;
      }

      return 5;

      break;

    case 6:
    case 7:
    case 8:
      if (spaceIndex < 3) {
        return 6;
      }

      if (spaceIndex < 6) {
        return 7;
      }

      return 8;

      break;
  }
}

export function sudokuToNumberLine(sudoku: number[][]) {
  let sudokuLine: number[] = [];
  for (let rowIndex = 0; rowIndex < 9; rowIndex += 3) {
    for (
      let blockNumberIndex = 0;
      blockNumberIndex < 9;
      blockNumberIndex += 3
    ) {
      const block_1 = sudoku[rowIndex];
      const block_2 = sudoku[rowIndex + 1];
      const block_3 = sudoku[rowIndex + 2];

      sudokuLine.push(
        block_1[blockNumberIndex],
        block_1[blockNumberIndex + 1],
        block_1[blockNumberIndex + 2]
      );
      sudokuLine.push(
        block_2[blockNumberIndex],
        block_2[blockNumberIndex + 1],
        block_2[blockNumberIndex + 2]
      );
      sudokuLine.push(
        block_3[blockNumberIndex],
        block_3[blockNumberIndex + 1],
        block_3[blockNumberIndex + 2]
      );
    }
  }
  return sudokuLine;
}

function countDistinct(arr: number[]) {
  let res = 1;

  for (let i = 1; i < arr.length; i++) {
    let j = 0;
    for (j = 0; j < i; j++) if (arr[i] === arr[j]) break;

    if (i === j) res++;
  }
  return res;
}

export function isSudokuResolved(blocks: number[][]) {
  let sudoku = getCompleteSudokuFromBlock(blocks);
  for (const block of sudoku.blocks) {
    if (
      block.includes(0) ||
      block.includes(undefined) ||
      countDistinct(block) !== 9
    ) {
      return false;
    }
  }

  for (const column of sudoku.columns) {
    if (
      column.includes(0) ||
      column.includes(undefined) ||
      countDistinct(column) !== 9
    ) {
      return false;
    }
  }

  for (const row of sudoku.rows) {
    if (
      row.includes(0) ||
      row.includes(undefined) ||
      countDistinct(row) !== 9
    ) {
      return false;
    }
  }

  return true;
}

async function resolveBlock(sudoku: app.sudoku.sudoku) {
  let resolveIndex = 0;
  let newSudoku: number[] = [];
  let newSudokuBlocks: number[][] = [];

  for (let rowIndex = 0; rowIndex < 9; rowIndex += 3) {
    for (let columnIndex = 0; columnIndex < 9; columnIndex += 3) {
      const rows = [
        sudoku.rows[rowIndex],
        sudoku.rows[rowIndex + 1],
        sudoku.rows[rowIndex + 2],
      ];
      const columns = [
        sudoku.columns[columnIndex],
        sudoku.columns[columnIndex + 1],
        sudoku.columns[columnIndex + 2],
      ];
      const sol = await getSolutionBlock(
        sudoku.blocks[resolveIndex],
        rows,
        columns
      );

      newSudoku = newSudoku.concat(
        ...sol.map((item) => (item.length > 1 ? 0 : item[0]))
      );
      newSudokuBlocks.push(newSudoku);
      newSudoku = [];
      resolveIndex += 1;
    }
  }

  return newSudokuBlocks;
}

async function getSolutionBlock(
  block: number[],
  rows: number[][],
  columns: number[][]
): Promise<number[][]> {
  const blockSolutions = [];
  const squareSolutions = [];
  let blockIndex = 0;
  for (let rowIndex = 0; rowIndex < 3; rowIndex++) {
    for (let columnIndex = 0; columnIndex < 3; columnIndex++) {
      if (block[blockIndex] === 0) {
        for (let numberIndex = 1; numberIndex < 10; numberIndex++) {
          if (
            isPossibleSolution(numberIndex, rows[rowIndex]) &&
            isPossibleSolution(numberIndex, columns[columnIndex]) &&
            isPossibleSolution(numberIndex, block)
          ) {
            squareSolutions.push(numberIndex);
          }
        }
      } else {
        squareSolutions.push(block[blockIndex]);
      }
      blockIndex += 1;
      blockSolutions.push([...squareSolutions]);
      squareSolutions.length = 0;
    }
  }
  return blockSolutions;
}

function isPossibleSolution(value: number, collection: number[]) {
  return !collection.includes(value);
}

export function initalizeSudoku(values: number[]): app.sudoku.sudoku {
  try {
    const sudoku: app.sudoku.sudoku = {
      rows: [],
      columns: [],
      blocks: [],
    };

    const newRealValuesColumns = [];

    for (let bigIndex = 0; bigIndex < 9; bigIndex++) {
      for (let smallIndex = 0; smallIndex < 81; smallIndex += 9) {
        newRealValuesColumns.push(values[bigIndex + smallIndex]);
      }

      sudoku.columns.push([...newRealValuesColumns]);

      newRealValuesColumns.length = 0;
    }

    const newRealValuesRows = [];

    for (let bigIndex = 0; bigIndex < 9; bigIndex++) {
      for (let index = 0; index < 9; index++) {
        newRealValuesRows.push(values[bigIndex * 9 + index]);
      }

      sudoku.rows.push([...newRealValuesRows]);

      newRealValuesRows.length = 0;
    }
    const realValuesBlocks = [];

    for (let rowsIndex = 0; rowsIndex < 56; rowsIndex += 27) {
      let columnCut = 0;
      for (let columnIndex = 0; columnCut < 3; columnIndex += 3) {
        realValuesBlocks.push(values[columnIndex + rowsIndex]);
        realValuesBlocks.push(values[columnIndex + rowsIndex + 1]);
        realValuesBlocks.push(values[columnIndex + rowsIndex + 2]);
        realValuesBlocks.push(values[columnIndex + rowsIndex + 9]);
        realValuesBlocks.push(values[columnIndex + rowsIndex + 1 + 9]);
        realValuesBlocks.push(values[columnIndex + rowsIndex + 2 + 9]);
        realValuesBlocks.push(values[columnIndex + rowsIndex + 18]);
        realValuesBlocks.push(values[columnIndex + rowsIndex + 1 + 18]);
        realValuesBlocks.push(values[columnIndex + rowsIndex + 2 + 18]);

        columnCut += 1;

        sudoku.blocks.push([...realValuesBlocks]);

        realValuesBlocks.length = 0;
      }
    }

    return sudoku;
  } catch (error) {
    console.log(error);
  }
}
